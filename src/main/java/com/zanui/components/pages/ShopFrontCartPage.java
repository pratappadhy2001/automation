package com.zanui.components.pages;

import com.aventstack.extentreports.Status;
import com.zanui.components.objects.ShopFrontCartPageObjects;
import com.zanui.components.objects.ShopFrontHomePageObjects;
import com.zanui.lib.main.BrowserActions;
import com.zanui.lib.main.Multimaplibraries;
import com.zanui.lib.utils.Reports;

import lombok.NoArgsConstructor;

/*************************************************************************
 * Objective: This represents the shopping cart page entity
 * Parameters:
 * Author: Pooja bagga
 **************************************************************************/


/**
* This class represents the cart page
* 
* @author Pooja Bagga
*
*/
@NoArgsConstructor
public class ShopFrontCartPage extends Multimaplibraries{
	static String className = ShopFrontProductDetailPage.class.getSimpleName();

	/**
	 * This method add product into cart
	 * 
	 * @param Scenario
	 */

	public static void ProceedToCheckout() {
		BrowserActions.isClick(ShopFrontCartPageObjects.checkoutButton);
		BrowserActions.isSleep();

		final boolean objVisible = BrowserActions.isVisible(ShopFrontHomePageObjects.myAccountLink);

		if (objVisible) {
			Reports.ExtentReportLog("AddProductToCart", Status.PASS, "Logout successful", true);
		} else {
			Reports.ExtentReportLog("AddProductToCart", Status.FAIL, "Logout Failed", true);
		}

	}
	
}
