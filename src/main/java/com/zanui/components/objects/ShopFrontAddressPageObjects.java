package com.zanui.components.objects;

/**
 * @author Pooja Bagga
 *
 */

public class ShopFrontAddressPageObjects {
	public static String firstNameTextbox = "id#shipping-address-first-name";
	public static String lastNameTextbox =	"id#shipping-address-last-name";
	public static String address1Textbox = "id#shipping-address-address1";
	public static String suburbTextbox = "id#shipping-address-suburb";
	public static String postcodeTextbox = "id#shipping-address-postcode";
	public static String phoneTextbox = "id#shipping-address-phone";
	public static String stateDropDown = "id#shipping-address-state";
	public static String continueButton = "xpath#//a[@data-behat='continue']";
}
