package com.zanui.components.objects;

/**
 * @author Pooja Bagga
 *
 */

public class ShopFrontPaymentPageObjects {

	public static String paymentInfoLabel = "xpath#//span[@class='checkout-step-text' and contains(text(),'Payment Info')]";
	public static String creditCardRadioButton = "id#//input[@id='option-credit-card']";
	public static String cardholderNameTextbox = "id#Registered-cardholder";
	public static String cardNumberTextbox = "id#Registered-cardnumber";
	public static String cardMonthDropdown = "xpath#//select[@data-behat='card-month']";
	public static String cardYearDropdown = "xpath#//select[@data-behat='card-year']";
	public static String CVCTextbox = "id#Registered-cvc";
	public static String billingEmailTextbox = "id#payment-email";
	public static String newsletterCheckbox = "id#newsletter-signup";
	public static String continuePaymentButton = "xpath//a[@data-behat='continue-payment']";	
}
